﻿using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication.Data
{
    public class TransactionService
    {
        private Transaction.Data.Repositories.TransactionRepository Transactions = new();
        public void SaveToDatabase(Transaction.Data.DatabaseModels.Transaction Transaction)
        {
            if (Transaction != null)
            {
                Transactions.SaveToDatabase(Transaction);
            }
        }
    }
}