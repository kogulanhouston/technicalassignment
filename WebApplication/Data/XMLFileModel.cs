﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace WebApplication.Data
{
    [Serializable]
    [XmlRoot(ElementName = "Transactions")]
    public class Transactions
    {
        [XmlElement("Transaction")]
        public List<TransactionsTransaction> colTransaction { get; set; } = new();
    }

    [Serializable]
    [XmlRoot(ElementName = "Transaction")]
    public class TransactionsTransaction
    {
        [XmlAttribute("id")]
        public string id { get; set; }
        [XmlElement("TransactionDate")]
        public System.DateTime TransactionDate { get; set; }
        [XmlElement("PaymentDetails")]
        public TransactionsTransactionPaymentDetails PaymentDetails { get; set; }
        [XmlElement("Status")]
        public string Status { get; set; }
    }

    [Serializable]
    [XmlRoot(ElementName = "PaymentDetails")]
    public partial class TransactionsTransactionPaymentDetails
    {
        [XmlElement("Amount")]
        public decimal Amount { get; set; }
        [XmlElement("CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}