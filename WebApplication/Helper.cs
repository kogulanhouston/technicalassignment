﻿using System;
using System.Globalization;
using System.Linq;
using WebApplication.Data;

namespace WebApplication
{
    public static class Helper
    {
        public static Transaction.Data.DatabaseModels.Transaction ToTransaction(this string Value)
        {
            if (!string.IsNullOrEmpty(Value) && Value.Contains(','))
            {
                string[] values = Value.Split(',');
                values.Validate();

                return new Transaction.Data.DatabaseModels.Transaction
                {
                    Id = values[0],
                    Amount = Convert.ToDecimal(values[1]),
                    CurrencyCode = values[2],
                    Date = GetDateTime(values[3]),
                    Status = values[4],
                };
            }
            else
            {
                return null;
            }
        }
        public static Transaction.Data.DatabaseModels.Transaction ToTransaction(this TransactionsTransaction Value)
        {
            if (Value != null)
            {
                return new Transaction.Data.DatabaseModels.Transaction
                {
                    Id = Value.id,
                    Amount = Convert.ToDecimal(Value.PaymentDetails.Amount),
                    CurrencyCode = Value.PaymentDetails.CurrencyCode,
                    Date = Value.TransactionDate,
                    Status = Value.Status,
                };
            }
            else
            {
                return null;
            }
        }
        private static void Validate(this string[] Values)
        {
            if (Values.Any(x => string.IsNullOrEmpty(x)))
            {
                throw new Exception("Invalid data.");
            }
        }
        private static DateTime GetDateTime(string value)
        {
            if (DateTime.TryParseExact(value, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
            {
                return dateTime;
            }
            else
            {
                throw new Exception("Invalid date time.");
            }
        }
    }
}
