﻿using System;
using System.Collections.Generic;

namespace Transaction.Data.Interfaces
{
    public interface ITransactionRepository
    {
        public IEnumerable<APIModels.Transaction> Get();
        public IEnumerable<APIModels.Transaction> GetTransactionByCurrencyCode(string CurrencyCode);
        public IEnumerable<APIModels.Transaction> GetTransactionByStatus(string Status);
        public IEnumerable<APIModels.Transaction> GetTransactionByDateRange(DateTime StartDate, DateTime EndDate);
        public void SaveToDatabase(DatabaseModels.Transaction Transaction);
    }
}
