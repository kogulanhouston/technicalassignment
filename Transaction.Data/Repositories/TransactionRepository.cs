﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Transaction.Data.Interfaces;

namespace Transaction.Data.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private const string connectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=Assignment;Integrated Security=True";
        private const string selectStatement = "SELECT T.[Id],CONCAT(T.[Amount],' ',T.[CurrencyCode]) AS [Payment],SM.[Value] AS [Status] FROM [dbo].[Transaction] AS T LEFT JOIN [dbo].[StatusMapping] AS SM ON SM.[Status] = T.[Status] ";

        public IEnumerable<APIModels.Transaction> Get()
        {
            IEnumerable<APIModels.Transaction> result = null;

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    result = con.Query<APIModels.Transaction>(selectStatement, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public IEnumerable<APIModels.Transaction> GetTransactionByCurrencyCode(string CurrencyCode)
        {
            IEnumerable<APIModels.Transaction> result = null;

            var sqlCmd = selectStatement + "WHERE [CurrencyCode] = @CurrencyCode ";

            var sqlParameters = new Dictionary<string, object>
            {
                { "@CurrencyCode", CurrencyCode }
            };

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    result = con.Query<APIModels.Transaction>(sqlCmd, sqlParameters, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public IEnumerable<APIModels.Transaction> GetTransactionByStatus(string Status)
        {
            IEnumerable<APIModels.Transaction> result = null;

            var sqlCmd = selectStatement + "WHERE [Value] = @Status";

            var sqlParameters = new Dictionary<string, object>
            {
                { "@Status", Status }
            };

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    result = con.Query<APIModels.Transaction>(sqlCmd, sqlParameters, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public IEnumerable<APIModels.Transaction> GetTransactionByDateRange(DateTime StartDate, DateTime EndDate)
        {
            IEnumerable<APIModels.Transaction> result = null;

            var sqlCmd = selectStatement + "WHERE [Date] BETWEEN @StartDate AND @EndDate";

            var sqlParameters = new Dictionary<string, object>
            {
                { "@StartDate", StartDate },
                { "@EndDate",  EndDate }
            };

            try
            {
                using (var con = new SqlConnection(connectionString))
                {
                    result = con.Query<APIModels.Transaction>(sqlCmd, sqlParameters, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public void SaveToDatabase(DatabaseModels.Transaction Transaction)
        {
            if (Transaction != null)
            {
                var sqlCmd = @"MERGE INTO [dbo].[Transaction] AS TARGET USING (
                             VALUES (@Id,@Date,@Status,@Amount,@CurrencyCode)) AS SOURCE 
                            ([Id],[Date],[Status],[Amount],[CurrencyCode]) ON 
                            TARGET.[Id] = SOURCE.[Id]
                            WHEN NOT MATCHED THEN 
                            INSERT ([Id],[Date],[Status],[Amount],[CurrencyCode])
                            VALUES (@Id,@Date,@Status,@Amount,@CurrencyCode);";

                using (var con = new SqlConnection(connectionString))
                {
                    con.Execute(sqlCmd, Transaction, commandType: CommandType.Text);
                }
            }
        }
    }
}
