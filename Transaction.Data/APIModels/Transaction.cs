﻿using System;
using System.Text.Json.Serialization;

namespace Transaction.Data.APIModels
{
    [Serializable]
    public class Transaction
    {
        public string Id { get; set; }
        public string Payment { get; set; }

        [JsonPropertyName("Status")]
        public string Status { get; set; }
    }
}
