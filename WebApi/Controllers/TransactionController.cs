﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private Transaction.Data.Repositories.TransactionRepository Transactions = new();

        [HttpGet]
        public IEnumerable<Transaction.Data.APIModels.Transaction> Get()
        {
            return Transactions.Get();
        }
        [HttpGet("byCurrencyCode/{currencyCode}")] //byCurrencyCode/USD
        public IEnumerable<Transaction.Data.APIModels.Transaction> GetTransactionByCurrencyCode(string CurrencyCode)
        {
            return Transactions.GetTransactionByCurrencyCode(CurrencyCode);
        }
        [HttpGet("byStatus/{status}")] //byStatus/R
        public IEnumerable<Transaction.Data.APIModels.Transaction> GetTransactionByStatus(string Status)
        {
            return Transactions.GetTransactionByStatus(Status);
        }
        [HttpGet("byDateRange/{startDate}/{endDate}")] //byDateRange/2019-01-01/2019-01-31 => yyyy/MM/dd
        public IEnumerable<Transaction.Data.APIModels.Transaction> GetTransactionByDateRange(DateTime StartDate, DateTime EndDate)
        {
            return Transactions.GetTransactionByDateRange(StartDate, EndDate);
        }
    }
}